import Web3 from 'web3';
import redis from 'redis';
import Promise, { promisifyAll } from 'bluebird';
import { ZeroClientProvider } from 'ddex-api';
import batchPromises from 'batch-promises';
import { curry, map, range } from 'ramda';
import cryptoKittiesABI from './ckabi';

const HTTP_URL = 'http://alpha-test.token.store:8555';
const WS_URL = 'wss://mainnet.infura.io/ws';
const CK_CONTRACT_ADDRESS = '0x06012c8cf97bead5deae237070f9587f8e7a266d';
const CK_OWNER = '0x081b834147d44b0740b668942cd28ae2963f3b1d';
const ME = '0xc40cd464ad0895571bb396071a4faa81935353a5';

const accounts = [ME];
const privateKeys = {
  [ME]: 'C8B6E2899C39FB0BB297FAC5562BEF2C9B2CA752D65B578BFF382E7790BD788C',
};

promisifyAll(redis.RedisClient.prototype);

const promisify = (inner) =>
  new Promise((resolve, reject) =>
    inner((err, res) => {
      if (err) return reject(err);
      resolve(res);
    })
  );

const main = async () => {
  // HTTP provider
  // const provider = ZeroClientProvider({
  //   rpcUrl: URL,
  //   getAccounts: function(cb) { cb(null, accounts) },
  //   getPrivateKey: function(address, cb) { cb(null, privateKeys[address]) }
  // });
  const provider = new Web3.providers.HttpProvider(HTTP_URL);
  const web3 = new Web3(provider);
  const contract = new web3.eth.Contract(cryptoKittiesABI, CK_CONTRACT_ADDRESS);
  const { methods } = contract;

  // WebSockets provider
  const wsProvider = new Web3.providers.WebsocketProvider(WS_URL);
  const wsWeb3 = new Web3(wsProvider);
  const wsContract = new wsWeb3.eth.Contract(cryptoKittiesABI, CK_CONTRACT_ADDRESS);
  
  // Redis client
  const client = redis.createClient();

  const ownerKey = ownerAddress => `kitties_for_${ownerAddress.toLowerCase()}`;

  const balanceOf = owner =>
    promisify(cb => methods.balanceOf(owner).call(cb));

  const tokensOfOwner = owner =>
    promisify(cb => methods.tokensOfOwner(owner).call(cb));

  const ownerOf = kittyId =>
    promisify(cb => methods.ownerOf(kittyId).call(cb));

  const transfer = (to, kittyToken) =>
    promisify(cb => methods.transfer(to, kittyToken).call(cb));

  const totalSupply = () =>
    promisify(cb => methods.totalSupply().call(cb));

  const printOwnerOf = async (kittyId) => {
    try {
      console.log(await ownerOf(kittyId));
    } catch (e) {
      console.log(`Error getting owner for kitty id = ${kittyId}`);
    }
  };

  const storeOwnerOf = curry(async (redisClient, startTime, kittyId) => {
    try {
      const ownerAddress = (await ownerOf(kittyId)).toLowerCase();
      const key = ownerKey(ownerAddress);
      console.log(`${kittyId} --> ${ownerAddress}, time offset: ${new Date().getTime() - startTime}`);
      return redisClient.sadd(key, kittyId);
    } catch(e) {
      console.log(`Error getting owner for kitty id = ${kittyId}`);
    }
  });

  const handleTransferKitty = curry(async (redisClient, event) => {
    try {
      const { to, from, tokenId } = event.returnValues;
      const fromKey = ownerKey(from);
      const toKey = ownerKey(to);
      console.log(`Transferring ${tokenId} ${from.toLowerCase()} --> ${to.toLowerCase()}`);
      await redisClient.srem(fromKey, tokenId);
      await redisClient.sadd(toKey, tokenId);
    } catch (e) {
      console.error('Error transferring kitty:', e);
    }
  });

  const transferEvents = wsContract.events.Transfer();
  transferEvents
    .on('data', handleTransferKitty(client))
    .on('error', console.error);

  const start = new Date().getTime();
  const startId = 1;
  // const startId = 318669;
  const lastId = (await totalSupply()) - 1;
  // const lastId = 318669;
  await batchPromises(100, range(startId, lastId + 1), storeOwnerOf(client, start));
  // console.log('owner of 318669:', await ownerOf(318669));

  const t = new Date().getTime() - start;
  console.log(`${t}ms`);
};

main();
