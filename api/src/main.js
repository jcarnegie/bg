import Web3 from 'web3';
import Promise, { promisifyAll } from 'bluebird';
import { createClient as createRedisClient, RedisClient } from 'redis';
import { GraphQLServer, PubSub, withFilter } from 'graphql-yoga';
import { curry, find, findIndex, map, prop, reverse, sortBy } from 'ramda';
import cryptoKittiesABI from './ckabi';

promisifyAll(RedisClient.prototype);

const WS_URL = 'wss://mainnet.infura.io/ws';
const CK_CONTRACT_ADDRESS = '0x06012c8cf97bead5deae237070f9587f8e7a266d';

const redis = createRedisClient({ host: 'redis' });
const provider = new Web3.providers.WebsocketProvider(WS_URL);
const web3 = new Web3(provider);
const contract = new web3.eth.Contract(cryptoKittiesABI, CK_CONTRACT_ADDRESS);
const transferEvents = contract.events.Transfer();

const ownerKey = ownerAddress => `kitties_for_${ownerAddress.toLowerCase()}`;

const giftHistory = async (owner) => {
  const key = `gift_history_list_${owner.toLowerCase()}`;
  const history = await redis.lrangeAsync(key, 0, 1000);
  return sortBy(prop('date'), map(JSON.parse, history));
};

const giftAndIndex = async (owner, to, kittyId) => {
  const key = `gift_history_list_${owner.toLowerCase()}`;
  const history = map(JSON.parse, await redis.lrangeAsync(key, 0, 1000));
  const equals = curry((to, kittyId, gift) => {
    return gift.to.toLowerCase() === to.toLowerCase()
      && gift.kittyId.toString() === kittyId.toString();
  });
  const gift = find(equals(to, kittyId), history);
  const idx = findIndex(equals(to, kittyId), history);
  return [gift, idx];
}

const main = async () => {
  const typeDefs = `
    type Gift {
      from: String!
      to: String!
      kittyId: Int!
      date: String
      status: String
    }

    type Query {
      cryptoKitties(owner: String!): [String]
      gifts(owner: String!): [Gift]
      fireKittyTransfer: Boolean
    }

    type Mutation {
      giftKitty(from: String!, to: String!, kittyId: Int!): Gift
    }

    type Subscription {
      kittyTransfer(owner: String!): [Gift]
    }
  `;

  const resolvers = {
    Query: {
      cryptoKitties: async (_, { owner }) => redis.smembersAsync(ownerKey(owner)),
      gifts: async (_, { owner }) => giftHistory(owner.toLowerCase()),
      fireKittyTransfer: async () => {
        const from = '0xc40cd464ad0895571bb396071a4faa81935353a5';
        const to = '0xb442984cb8c7eaed7fb9747fe7d58ba07648e880';
        const tokenId = 501320;
        const [gift, idx] = await giftAndIndex(from.toLowerCase(), to.toLowerCase(), tokenId);
        console.log(gift, idx);
        if (!gift) return false;
        gift.status = 'Gifted';
        const key = `gift_history_list_${from.toLowerCase()}`;
        const val = JSON.stringify(gift);
        await redis.lsetAsync(key, idx, val);
        const history = await giftHistory(from.toLowerCase());
        pubsub.publish(from.toLowerCase(), { kittyTransfer: history || [] });
        return true;
      }
    },
    Mutation: {
      giftKitty: async (_, { from, to, kittyId }) => {
        try {
          const date = new Date();
          const status = 'Pending';
          const key = `gift_history_list_${from.toLowerCase()}`;
          const gift = {
            from: from.toLowerCase(),
            to: to.toLowerCase(),
            kittyId,
            status,
            date
          };
          const val = JSON.stringify(gift);
          await redis.rpushAsync(key, val);
          return gift;
        } catch (e) {
          return false;
        }
      }
    },
    Subscription: {
      kittyTransfer: {
        // subscribe: withFilter((_, { owner }) => pubsub.asyncIterator(owner), (payload, variables) => {
        //   console.log('kittyTransfer filter:', variables, payload);
        //   payload.kittyTransfer[0].from === variables.owner.toLowerCase();
        // }),
        subscribe: (_, { owner }) => pubsub.asyncIterator(owner.toLowerCase()),
      },
    }
  };

  const pubsub = new PubSub();
  const server = new GraphQLServer({ typeDefs, resolvers, context: { pubsub } });
  const config = {
    cors: {
      "origin": "*",
      "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
      "preflightContinue": false,
      "optionsSuccessStatus": 204
    }
  };
  server.start(config, () => console.log('Server is running on localhost:4000'));

  transferEvents.on('data', async (event) => {
    const { from, to, tokenId } = event.returnValues;
    const [gift, idx] = await giftAndIndex(from.toLowerCase(), to.toLowerCase(), tokenId);
    if (!gift) return;
    gift.status = 'Gifted';
    console.log('Gift transfer:', from, to, tokenId, gift);
    const key = `gift_history_list_${from.toLowerCase()}`;
    const val = JSON.stringify(gift);
    console.log('Gift transfer:', key, val, idx);
    await redis.lsetAsync(key, idx, val);
    const history = await giftHistory(from.toLowerCase());
    console.log(`Publishing updated gift history to ${from.toLowerCase()}`, history);
    pubsub.publish(from.toLowerCase(), { kittyTransfer: history || [] });
  });
};

main();
