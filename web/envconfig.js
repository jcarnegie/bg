module.exports = {
  'process.env.NODE_ENV':         process.env.NODE_ENV,
  'process.env.API_SERVICE_HOST': process.env.API_SERVICE_HOST,
  'process.env.API_SERVICE_PORT': process.env.API_SERVICE_PORT,
};
