import React, { Component } from 'react';
import autobind from 'autobind-decorator';

const kittyUrl = id =>
  `https://storage.googleapis.com/ck-kitty-image/0x06012c8cf97bead5deae237070f9587f8e7a266d/${id}.svg`;

export default class GiftItem extends Component {
  render() {
    const { gift } = this.props;
    return (
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
        <button>{gift.status}</button>
        <img width="100" height="100" src={kittyUrl(gift.kittyId)} />
        <span style={{ fontSize: 11 }}>--></span>
        <span style={{ fontSize: 11 }}>{ gift.to }</span>
      </div>
    );
  }
}
