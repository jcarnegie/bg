import gql from 'graphql-tag';
import Promise from 'bluebird';
import autobind from 'autobind-decorator';
import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { find, propEq } from 'ramda';
import cryptoKittiesABI from '../lib/ckabi';

const ETH_URL = 'http://alpha-test.token.store:8555';
const CK_CONTRACT_ADDRESS = '0x06012c8cf97bead5deae237070f9587f8e7a266d';

const giftKitty = gql`
  mutation giftKitty($from: String!, $to: String!, $kittyId: Int!) {
    giftKitty(from: $from, to: $to, kittyId: $kittyId) {
      from to kittyId date status
    }
  }
`;

const giftIconStyle = {
  // alignSelf: 'flex-end',
};

const listItemStyle = {
  alignItems: 'center',
  display: 'flex',
  justifyContent: 'space-between',
};

const buttonStyle = {
  marginLeft: 10,
};

const inputStyle = {
  textAlign: 'center',
  width: 250,
};

const promisify = inner =>
  new Promise((resolve, reject) =>
    inner((err, res) => {
      if (err) return reject(err);
      resolve(res);
    }));

const kittyUrl = id =>
  `https://storage.googleapis.com/ck-kitty-image/0x06012c8cf97bead5deae237070f9587f8e7a266d/${id}.svg`;

export default class KittyListItem extends Component {
  constructor(props) {
    super(props);

    const Contract = web3.eth.contract(cryptoKittiesABI);
    const contract = Contract.at(CK_CONTRACT_ADDRESS);

    const transfer = (to, kittyToken) =>
      promisify(cb => contract.transfer(to, kittyToken, cb));

    this.state = {
      toAddress: '',
      showGiftForm: false,
      transfer,
    };
  }

  @autobind
  handleFieldChange(field) {
    return e => this.setState({ [field]: e.target.value });
  }

  @autobind
  handleSubmit(giftKitty) {
    return async (event) => {
      event.preventDefault();

      const { from, kittyId } = this.props;
      const { toAddress, transfer } = this.state;

      await transfer(toAddress, kittyId);

      const r2 = await giftKitty({ variables: {
        from,
        to: toAddress,
        kittyId: parseInt(kittyId, 10),
      }});
      this.props.onGift(r2.data.giftKitty);
      this.setState({ showGiftForm: false });
    };
  }

  @autobind
  handleToggleGiftForm() {
    this.setState({ showGiftForm: !this.state.showGiftForm });
  }

  render() {
    const { gifts, kittyId } = this.props;
    const cmp = g => parseInt(kittyId, 10) === parseInt(g.kittyId, 10) && g.status === 'Pending';
    const gift = find(cmp, gifts);
    const isPendingTransfer = (gift != null);
    return (
      <Mutation mutation={giftKitty}>
        { giftKitty => (
          <div style={listItemStyle}>
            <img width="100" height="100" src={kittyUrl(kittyId)} />
            { this.state.showGiftForm ? (
              <form onSubmit={this.handleSubmit(giftKitty)}>
                <div style={{ display: 'flex' }}>
                  <input id="owner" style={inputStyle} type="text" placeholder="Enter address to gift" onChange={this.handleFieldChange('toAddress')} value={this.state.toAddress} />
                  <button style={buttonStyle} type="submit">Gift</button>
                  <button style={buttonStyle} onClick={this.handleToggleGiftForm}>Cancel</button>
                </div>
              </form>
            ) : (
              <span onClick={this.handleToggleGiftForm}>
                {
                  !isPendingTransfer &&
                    <i style={giftIconStyle} className="fas fa-gift" />
                }
              </span>
            )}
          </div>
        )}
      </Mutation>
    );
  }
}
