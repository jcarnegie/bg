import React, { Component } from 'react';
import withRedux from 'next-redux-wrapper';
import autobind from 'autobind-decorator';
import gql from 'graphql-tag';
import {
  compose,
  difference,
  filter,
  isEmpty,
  map,
  not,
  prepend,
  prop,
  propEq,
  toString,
} from 'ramda';
import { ApolloConsumer } from 'react-apollo';
import { initStore } from '../lib/store';
import App from '../components/App';
import Header from '../components/Header';
import KittyListItem from '../components/KittyListItem';
import GiftItem from '../components/GiftItem';
import withApollo from '../lib/withApollo';

const listStyle = {
  borderTop: 'solid 1px #DDD',
  listStyleType: 'none',
  margin: 0,
  marginTop: 30,
  padding: 0,
  width: 500,
};

const listItemStyle = {
  borderBottom: 'solid 1px #DDD',
  padding: 10,
};

const buttonStyle = {
  marginLeft: 10,
};

const inputStyle = {
  textAlign: 'center',
  width: 445,
};

const headerStyle = {
  textAlign: 'center',
  width: 500,
};

const cryptoKittiesQuery = gql`
  query cryptoKitties($owner: String!) {
    cryptoKitties(owner: $owner)
    gifts(owner: $owner) {
      from to kittyId date status
    }
  }
`;

const giftsSubscription = gql`
  subscription kittyTransfer($owner: String!) {
    kittyTransfer(owner: $owner) {
      from to status date kittyId
    }
  }
`;

const propNotEq = compose(not, propEq);

const KittyList = ({
  from,
  gifts,
  kittyIds,
  onGift,
}) =>
  map(id => (
    <li key={id} style={listItemStyle}>
      <KittyListItem from={from} gifts={gifts} kittyId={id} onGift={onGift} />
    </li>
  ), kittyIds);

const GiftList = ({ gifts }) =>
  map(gift => (
    <li key={gift.date} style={listItemStyle}>
      <GiftItem gift={gift} />
    </li>
  ), gifts);

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ownerAddress: (typeof web3 !== 'undefined') ? web3.eth.accounts[0] : '',
      cryptoKitties: [],
      gifts: [],
    };
  }

  async componentDidMount() {
    const { ownerAddress } = this.state;
    const { client } = this.props;
    if (isEmpty(this.state.ownerAddress)) return;

    const sub = client.subscribe({
      query: giftsSubscription,
      variables: { owner: ownerAddress },
    });

    sub.subscribe({
      next: ({ data: { kittyTransfer } }) => {
        const gifts = kittyTransfer;
        const giftedKittyIds = map(prop('id'), filter(propEq('status', 'Gifted'), gifts));
        const cryptoKitties = difference(this.state.cryptoKitties, giftedKittyIds);
        console.log('gifts update:', gifts);
        this.setState({ cryptoKitties, gifts });
      },
    });

    this.fetchData();
  }

  async fetchData() {
    const { client } = this.props;
    const { data } = await client.query({
      query: cryptoKittiesQuery,
      variables: { owner: this.state.ownerAddress },
    });
    const { cryptoKitties, gifts } = data;
    const giftedKittyIds = map(toString, map(prop('kittyId'), filter(propEq('status', 'Gifted'), gifts)));
    const filteredKitties = difference(cryptoKitties, giftedKittyIds);

    console.log('cryptoKitties vs filtered:', cryptoKitties, filteredKitties, giftedKittyIds, gifts);

    this.setState({ cryptoKitties: filteredKitties, gifts });
  }

  @autobind
  handleFieldChange(field) {
    return e => this.setState({ [field]: e.target.value });
  }

  @autobind
  handleSubmit(e) {
    e.preventDefault();
    this.fetchData();
  }

  @autobind
  handleGift(gift) {
    const { gifts } = this.state;
    this.setState({ gifts: prepend(gift, gifts) });
  }

  render() {
    return (
      <div>
        <ApolloConsumer>
          { client => (
            <App>
              <Header />
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div style={{ width: 500 }}>
                  <form onSubmit={this.handleSubmit}>
                    <h3 style={headerStyle} >CryptoKitties</h3>
                    <div style={{ display: 'flex' }}>
                      <input id="owner" style={inputStyle} type="text" placeholder="Enter owner's address" onChange={this.handleFieldChange('ownerAddress')} value={this.state.ownerAddress} />
                      <button style={buttonStyle} type="submit">Show</button>
                    </div>
                  </form>
                  <ul style={listStyle}>
                    <KittyList
                      from={this.state.ownerAddress}
                      gifts={this.state.gifts}
                      kittyIds={this.state.cryptoKitties}
                      onGift={this.handleGift}
                    />
                  </ul>
                </div>
                <div style={{ width: 500 }}>
                  <h3 style={headerStyle}>Gift History</h3>
                  <ul style={listStyle}>
                    <GiftList gifts={this.state.gifts} />
                  </ul>
                </div>
              </div>
            </App>
          )}
        </ApolloConsumer>
      </div>
    );
  }
}

export default withApollo(withRedux(initStore)(Index));
