/* @flow weak */
import React, { Component } from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { map } from 'ramda';
import withApollo from '../lib/withApollo';
import App from '../components/App';

export const accounts = gql`
  { accounts { id email } }
`;

class Accounts extends Component {
  render() {
    console.log('this.props.data:', this.props.data);
    const { accounts, error, loading } = this.props.data;
    // console.log('loading:', this.props.data.loading);
    // console.log('error:', this.props.data.error);
    // console.log('accounts:', this.props.data.accounts);
    const fn = acct => (<div key={Math.random()}><p>{ acct.id }</p><p>{ acct.email }</p></div>);
    return (
      <App>{ !loading && !error && map(fn, accounts) }</App>
    );
  }
}

export default withApollo(graphql(accounts)(Accounts));
